﻿namespace TestMultithread
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LongArithmetic;

    /// <summary>
    /// </summary>
    class Program
    {
        /// <summary>
        /// </summary>
        /// <param name="args">
        /// </param>
        static void Main(string[] args)
        {
            var testNumbers = new HashSet<string>();

            for (int i = 0; i < 100; i++)
            {
                testNumbers.Add(GetNumberString());
            }
            
            Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA";
            foreach (var testNumber in testNumbers)
            {
                var numberString = testNumber;
                Accumulator.Increment(numberString);
            }

            var resultOneString = Accumulator.NumberLetterIdetntificator;

            Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA";
            Parallel.ForEach(
                testNumbers,
                a =>
                    {
                        var numberString = a;
                        Accumulator.Increment(numberString);
                    });

            var resultMultiString = Accumulator.NumberLetterIdetntificator;

            if (resultOneString == resultMultiString)
            {
                Console.WriteLine("{0} - {1} - {2}", "ok", resultOneString, resultOneString);
            }
            else
            {
                Console.WriteLine("{0} - {1} - {2}", "error", resultOneString, resultMultiString);

            }

            Console.ReadLine();
            Environment.Exit(0);
        }

        /// <summary>
        /// </summary>
        private static Random random = RandomProvider.GetThreadRandom();
       
        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        private static string GetNumberString()
        {
           var v = random.Next(1000000);
            return v.ToString();
            // return 1.ToString();
        }
    }
}
