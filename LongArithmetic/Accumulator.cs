﻿namespace LongArithmetic
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;

    /// <summary>
    /// </summary>
    public class Accumulator
    {
        /// <summary>
        /// </summary>
        private static string numberLetterIdetntificator;

        /// <summary>
        /// </summary>
        public static string NumberLetterIdetntificator
        {
            get
            {
                return numberLetterIdetntificator;
            }

            set
            {
                numberLetterIdetntificator = value;
                
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="value">
        /// </param>
        public static void Increment(string value)
        {
            if (!IsSymbolValid(value))
            {
                return;
            }

            string idf;

            string newVal;

            do
            {
                 idf = numberLetterIdetntificator;

                if (string.IsNullOrEmpty(idf))
                {
                    return;
                }

                string p2 = value.Trim();

                string p1 = "0";
                int tailLength = 0;

                p1 = GetP1Number(idf, p1, ref tailLength);

                var sum = Sum(p1, p2);
                newVal = InsertFinalDigit(idf, sum, tailLength);
            }
            while (Interlocked.CompareExchange(ref numberLetterIdetntificator, newVal, idf) != idf);
        }

        /// <summary>
        /// </summary>
        /// <param name="data">
        /// </param>
        /// <param name="value">
        /// </param>
        /// <param name="tailLength">
        /// </param>
        /// <returns>
        /// </returns>
        public static string InsertFinalDigit(string data, string value, int tailLength)
        {
            var startIndex = data.Length - tailLength;

            var remove = data.Remove(startIndex, tailLength);

            return remove + value;
        }

        /// <summary>
        /// </summary>
        /// <param name="first">
        /// </param>
        /// <param name="second">
        /// </param>
        /// <returns>
        /// </returns>
        public static string Sum(string first, string second)
        {
            AddLeadingZero(ref first, ref second);

            var i = first.Length;

            var result = new char[i + 1 + 1];

            int vUme1;

            result[0] = '0';

            for (vUme1 = 0; i > 0; i--)
            {
                var p1 = first[i - 1] - 48;
                var p2 = second[i - 1] - 48;

                var tmp = p1 + p2 + vUme1;

                if (tmp >= 10)
                {
                    vUme1 = 1;
                    tmp -= 10;
                }
                else
                {
                    vUme1 = 0;
                }

                var c = Convert.ToChar(tmp + 48);

                result[i] = c;
            }

            if (vUme1 == 1)
            {
                result[0] = (char)(result[0] + 1);
            }

            var sum = new string(result).TrimEnd('\0');

            return result[0] != '0' ? sum : sum.Substring(1);
        }

        /// <summary>
        /// </summary>
        /// <param name="idf">
        /// </param>
        /// <param name="p1">
        /// </param>
        /// <param name="tailLength">
        /// </param>
        /// <returns>
        /// </returns>
        private static string GetP1Number(string idf, string p1, ref int tailLength)
        {
            var lastDigit = idf.Last();
            if (!char.IsDigit(lastDigit))
            {
                return p1;
            }

            string[] numbers = Regex.Split(idf, @"\D+");
            p1 = numbers[numbers.Length - 1];
            tailLength = p1.Length;
            return p1;
        }

        /// <summary>
        /// </summary>
        /// <param name="str">
        /// </param>
        /// <returns>
        /// </returns>
        private static bool IsSymbolValid(string str)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(str);
        }

        /// <summary>
        /// </summary>
        /// <param name="first">
        /// </param>
        /// <param name="second">
        /// </param>
        private static void AddLeadingZero(ref string first, ref string second)
        {
            var l = first.Length - second.Length;

            if (l < 0)
            {
                first = first.PadLeft(first.Length + Math.Abs(l), '0');
            }
            else if (l > 0)
            {
                second = second.PadLeft(second.Length + Math.Abs(l), '0');
            }
        }
    }
}