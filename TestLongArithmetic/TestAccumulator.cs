﻿namespace TestLongArithmetic
{
    using System;

    using LongArithmetic;

    using NUnit;
    using NUnit.Framework;

     [TestFixture(Description = "Tests for Accumulator")]
    public class TestAccumulator
    {
        [Test]
       public void TestInsertDigit()
        {
            Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA";
           var test = Accumulator.InsertFinalDigit(Accumulator.NumberLetterIdetntificator, "123", 0);
           Assert.IsTrue(test == "345FAS310E575896325SA123");

           Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA123";
           test = Accumulator.InsertFinalDigit(Accumulator.NumberLetterIdetntificator, "357", 3);
           Assert.IsTrue(test == "345FAS310E575896325SA357");

        }

         [Test]
         public void TestIncrement()
         {
             Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA";
             Accumulator.Increment("123");

             Assert.IsTrue(Accumulator.NumberLetterIdetntificator == "345FAS310E575896325SA123");

             Accumulator.NumberLetterIdetntificator = "345FAS310E575896325SA123";
             Accumulator.Increment("234");
             Assert.IsTrue(Accumulator.NumberLetterIdetntificator == "345FAS310E575896325SA357");
         }

         [Test]
         public void TestSumm()
         {
             var sum = Accumulator.Sum("123", "234");

             Assert.IsTrue(sum == "357");

             sum = Accumulator.Sum("12", "7");

             Assert.IsTrue(sum == "19");

             sum = Accumulator.Sum("0", "7");
             Assert.IsTrue(sum == "7");

             sum = Accumulator.Sum("999", "999");
             Assert.IsTrue(sum == "1998");

             sum = Accumulator.Sum("999", "1");
             Assert.IsTrue(sum == "1000");

             sum = Accumulator.Sum("0", "1");
             Assert.IsTrue(sum == "1");


             sum = Accumulator.Sum("0", "0");
             Assert.IsTrue(sum == "0");

         }
    }
}