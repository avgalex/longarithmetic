﻿namespace WindowsFormsTest
{
    using System;
    using System.Windows.Forms;

    using LongArithmetic;

    /// <summary>
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        private void IncrementClick(object sender, EventArgs e)
        {
            var numberLetterIdetntificator = this.txtIdentificator.Text;
            var value = this.txtParametr.Text;

            if (string.IsNullOrEmpty(numberLetterIdetntificator) && string.IsNullOrEmpty(value))
            {
                return;
            }

            Accumulator.NumberLetterIdetntificator = numberLetterIdetntificator;
            Accumulator.Increment(value);
            this.txtIdentificator.Text = Accumulator.NumberLetterIdetntificator;
        }
    }
}
