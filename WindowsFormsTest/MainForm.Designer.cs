﻿namespace WindowsFormsTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblParametr = new System.Windows.Forms.Label();
            this.txtIdentificator = new System.Windows.Forms.TextBox();
            this.txtParametr = new System.Windows.Forms.TextBox();
            this.Increment = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.23587F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.76413F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblParametr, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtIdentificator, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtParametr, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Increment, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.63877F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.92401F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.90224F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.53498F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(814, 497);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Идентификатор";
            // 
            // lblParametr
            // 
            this.lblParametr.AutoSize = true;
            this.lblParametr.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblParametr.Location = new System.Drawing.Point(3, 185);
            this.lblParametr.Name = "lblParametr";
            this.lblParametr.Size = new System.Drawing.Size(175, 26);
            this.lblParametr.TabIndex = 1;
            this.lblParametr.Text = "Параметр";
            // 
            // txtIdentificator
            // 
            this.txtIdentificator.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtIdentificator.Location = new System.Drawing.Point(184, 85);
            this.txtIdentificator.Name = "txtIdentificator";
            this.txtIdentificator.Size = new System.Drawing.Size(627, 31);
            this.txtIdentificator.TabIndex = 2;
            // 
            // txtParametr
            // 
            this.txtParametr.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtParametr.Location = new System.Drawing.Point(184, 188);
            this.txtParametr.Name = "txtParametr";
            this.txtParametr.Size = new System.Drawing.Size(627, 31);
            this.txtParametr.TabIndex = 3;
            // 
            // Increment
            // 
            this.Increment.Dock = System.Windows.Forms.DockStyle.Top;
            this.Increment.Location = new System.Drawing.Point(184, 267);
            this.Increment.Name = "Increment";
            this.Increment.Size = new System.Drawing.Size(627, 41);
            this.Increment.TabIndex = 4;
            this.Increment.Text = "+";
            this.Increment.UseVisualStyleBackColor = true;
            this.Increment.Click += new System.EventHandler(this.IncrementClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 497);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Test";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblParametr;
        private System.Windows.Forms.TextBox txtIdentificator;
        private System.Windows.Forms.TextBox txtParametr;
        private System.Windows.Forms.Button Increment;
    }
}

